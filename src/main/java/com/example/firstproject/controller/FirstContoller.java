package com.example.firstproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class FirstContoller {

    public FirstContoller() {
    }

    @GetMapping("/hi")
    public  String SampleMain(Model model){

        model.addAttribute("username", "테스터");
        return "greeting";
    }

    @GetMapping("/bye")
    public String seeYouNext(Model model){
        model.addAttribute("nickname" ,"bybynick");
        return "goodbye";
    }
}
